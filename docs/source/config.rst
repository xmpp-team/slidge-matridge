Configuration
=============

Setting up a slidge component
-----------------------------

Refer to the `slidge admin docs <https://slidge.im/docs/slidge/main/admin>`_ for generic
instructions on how to set up a slidge component, and for slidge core
configuration options.

matridge-specific config
------------------------

.. config-obj:: matridge.config
